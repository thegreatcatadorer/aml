type register =
  | RAX
  | RCX
  | RDX
  | RBX
  | RSI
  | RDI
  | R08
  | R09
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15

let reg_num = function
  | RAX -> 0
  | RCX -> 1
  | RDX -> 2
  | RBX -> 3
  | RSI -> 4
  | RDI -> 5
  | R08 -> 6
  | R09 -> 7
  | R10 -> 8
  | R11 -> 9
  | R12 -> 10
  | R13 -> 11
  | R14 -> 12
  | R15 -> 13

let reg_pos = function
  | RAX -> 0x0b8
  | RCX -> 0x0b9
  | RDX -> 0x0ba
  | RBX -> 0x0bb
  | RSI -> 0x0be
  | RDI -> 0x0bf
  | R08 -> 0x1b8
  | R09 -> 0x1b9
  | R10 -> 0x1ba
  | R11 -> 0x1bb
  | R12 -> 0x1bc
  | R13 -> 0x1bd
  | R14 -> 0x1be
  | R15 -> 0x1bf

open Ssa

let ranges (Func (initial, statements)) =
  let uses = Hashtbl.create 0 in
  let init_var i _ = Hashtbl.add uses i (Base.Queue.create ()) in
  Array.iteri init_var initial;
  let rec loop i =
    if i != 0 then
      let i = i - 1 in
      match statements.(i) with
      | Assignment (Var v, Expression (_, operands)) ->
          init_var v ();
          let each (Var v) =
            let v_uses = Hashtbl.find uses v in
            Base.Queue.enqueue v_uses i
          in
          List.iter each operands
      | _ -> ()
  in
  loop (Array.length statements);
  uses

let linear_alloc (Func (initial, statements) as func) =
  let ranges = ranges func
  and registers = Array.make 13 () in
  let rec loop i =
    if i != 0 then
      let i = i - 1 in
      let stmt = statements.(i) in
