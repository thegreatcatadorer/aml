type token_type =
  | LeftParen
  | RightParen
  | LeftBracket
  | RightBracket
  | LeftCurly
  | RightCurly
  | Atom
  | Keyword
  | Comment
  | Character
  | Quote
  | Quasiquote
  | Unquote
  | String
  | Annotate
  | UseTable

type property = Parser

type expression =
  | Int of int
  | Float of float
  | Char of Uchar.t
  | String of string
  | Template of expression list
  | Symbol of string
  | Keyword of expression
  | Quote of expression
  | Quasiquote of expression
  | Unquote of expression
  | Source of expression * int * int
  | Annotation of expression * expression
  | Comment of expression * expression
  | Property of expression * expression
  | List of expression list
  | Vec of expression list
  | Map of expression list

type 'a error =
  | ErrorGroup of 'a
  | ExpectedNumber of 'a * int
  | TableError of 'a * token_type
  | TableTypeError of 'a * expression
  | NotFound of 'a * string
  | Unimplemented

module type input = sig
  type t

  (* Returns the value of the character at the chosen byte offset. *)
  val ( .:[] ) : t -> int -> char

  (* Returns a slice of the input up to the point. Cannot be safely modified. *)
  val ( .:<[] ) : t -> int -> string
end

let list exprs = List exprs
let vec exprs = Vec exprs
let map exprs = Map exprs
let keyword expr = Keyword expr
let quote expr = Quote expr
let quasiquote expr = Quasiquote expr
let unquote expr = Unquote expr
let ( =? ) uchar char = uchar = Uchar.of_char char
let ( =!? ) uchar char = uchar != Uchar.of_char char

let digit_val char max =
  let unbounded_digit_val =
    if '0' <= char && char <= '9' then Char.code char - 48
    else if 'A' <= char && char <= 'Z' then Char.code char - 55
    else if 'a' <= char && char <= 'z' then Char.code char - 87
    else -1
  in
  if max <= unbounded_digit_val then -1 else unbounded_digit_val

module Parser (Input : input) = struct
  open Input

  type state = { input : Input.t; pos : int }
  type parser = state -> (state * expression, state error) result
  type env = (string * (property * parser) list) Base.Queue.t

  (*
Derived from https://github.com/rust-lang/rust/blob/fc11ee02ee91b32e23684cd478bca80fe5323b47/library/core/src/str/validations.rs
MIT License
Copyright (c) 2023 Rust contributors
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*)
  let next_uchar { input; pos } =
    let x = Char.code input.:[pos] in
    if x <= 0x7F then ({ input; pos = pos + 1 }, Uchar.of_int x)
    else
      let init = x land 0x1F in
      let y = Char.code input.:[pos + 1] in
      let len, result =
        if x <= 0xDF then (2, (init lsl 6) lor (y land 0x3F))
        else
          let z = Char.code input.:[pos + 2] in
          let yz = ((y land 0x3F) lsl 6) lor (z land 0x3F) in
          if x <= 0xEF then (3, (init lsl 12) lor yz)
          else
            let w = Char.code input.:[pos + 3] in
            (4, ((init land 7) lsl 18) lor ((yz lsl 6) lor (w land 0x3F)))
      in
      ({ input; pos = pos + len }, Uchar.of_int result)

  let parse_digits radix state =
    let rec parse_digits_acc state acc =
      let state1, char = next_uchar state in
      if not (Uchar.is_char char) then state
      else
        let char = Uchar.to_char char in
        let value = digit_val char radix in
        if value >= 0 then (
          Buffer.add_char acc (Char.chr value);
          parse_digits_acc state1 acc)
        else if char = '_' then parse_digits_acc state1 acc
        else state
    in
    let acc = Buffer.create 4 in
    let state = parse_digits_acc state acc in
    (state, acc)

  let parse_nat radix state =
    let state, buffer = parse_digits radix state in
    if Buffer.length buffer = 0 then None
    else
      (* Parse_digits converts characters to their values *)
      (* Due to the offset we don't have to convert again *)
      let fold i c = (i * radix) + Char.code c in
      Some (state, Seq.fold_left fold 0 (Buffer.to_seq buffer))

  let parse_sign state =
    let state1, char = next_uchar state in
    if char =? '-' then (state1, -1)
    else if char =? '+' then (state1, 1)
    else (state, 1)

  let parse_int radix state =
    let state, sign = parse_sign state in
    match parse_nat radix state with
    | None -> None
    | Some (state, i) -> Some (state, i * sign)

  let parse_float radix state =
    let radix_fl = Int.to_float radix
    and state, buffer = parse_digits radix state in
    if Buffer.length buffer = 0 then None
    else
      (* Parse_digits converts characters to their values *)
      (* Due to the offset we don't have to convert again *)
      let acc = ref 0. and max = Buffer.length buffer in
      (* Iterating in reverse order *)
      let rec loop i =
        if i != max then (
          let i = i + 1 in
          let c = Buffer.nth buffer (max - i) in
          acc := (!acc +. Int.to_float (Char.code c)) /. radix_fl;
          loop i)
      in
      loop 0;
      Some (state, !acc)

  let parse_num radix state =
    let state, sign = parse_sign state in
    match parse_nat radix state with
    | None -> None
    | Some (state, i) ->
        let state1, char = next_uchar state in
        let state, f =
          if char =!? '.' then (state, None)
          else
            match parse_float radix state1 with
            | None -> (state, None)
            | Some (state, f) -> (state, Some f)
        in
        let state1, char = next_uchar state in
        let state, e =
          if char =!? 'e' && char =!? 'E' then (state, None)
          else
            match parse_int radix state1 with
            | None -> (state, None)
            | Some (state, e) -> (state, Some e)
        in
        if Option.is_none f && Option.is_none e then Some (state, Int i)
        else
          let f = Option.value f ~default:0.
          and e = Option.value e ~default:0 in
          let n = Int.to_float sign *. (Int.to_float i +. f)
          and e = Float.pow (Int.to_float radix) (Int.to_float e) in
          Some (state, Float (n *. e))

  let parse_symbol state =
    let rec is_valid_char char =
      if Uucp.White.is_white_space char then false
      else if not (Uchar.is_char char) then true
      else
        match Uchar.to_char char with
        | '~' | '`' | '\'' | '\\' | ':' | ',' | '#' | '^' -> false
        | '(' | ')' | '[' | ']' | '{' | '}' -> false
        | _ -> true
    and parse_symbol_acc state acc =
      let state1, char = next_uchar state in
      if is_valid_char char then (
        Buffer.add_utf_8_uchar acc char;
        parse_symbol_acc state1 acc)
      else state
    and acc = Buffer.create 4 in
    let state = parse_symbol_acc state acc in
    (state, Symbol (Buffer.contents acc))

  let parse_atom state =
    match parse_num 10 state with
    | Some with_num -> with_num
    | None -> parse_symbol state

  (* Accepts the portion of a string following the initial "". *)
  let rec parse_string env state =
    let rec parse_chars state buffer =
      let state1, char = next_uchar state in
      if char =? '"' || char =? '\\' then state
      else (
        Buffer.add_utf_8_uchar buffer char;
        parse_chars state1 buffer)
    and parse_escape state =
      let state, _ = next_uchar state in
      let state1, char = next_uchar state in
      if char =? '(' then parse_toplevel env state else Ok (state1, Char char)
    and parse_string_acc state acc =
      let state1, char = next_uchar state in
      if char =? '"' then Ok state1
      else if char =? '\\' then
        match parse_escape state with
        | Ok (state, result) ->
            Base.Queue.enqueue acc result;
            parse_string_acc state acc
        | Error _ as e -> e
      else
        let buffer = Buffer.create 4 in
        let state = parse_chars state buffer in
        Base.Queue.enqueue acc (String (Buffer.contents buffer));
        parse_string_acc state acc
    in
    let acc = Base.Queue.create () in
    let default () = (state, Template (Base.Queue.to_list acc)) in
    match parse_string_acc state acc with
    | Ok state ->
        Ok
          (if Base.Queue.length acc = 1 then
           let first = Base.Queue.get acc 0 in
           if match first with String _ -> true | _ -> false then (state, first)
           else default ()
          else default ())
    | Error _ as e -> e

  and next_token state =
    let state1, char = next_uchar state in
    if Uucp.White.is_white_space char || char =? ',' then next_token state1
    else if not (Uchar.is_char char) then (state, Atom)
    else
      let char = Uchar.to_char char in
      let token_type =
        if char = '(' then LeftParen
        else if char = ')' then RightParen
        else if char = '[' then LeftBracket
        else if char = ']' then RightBracket
        else if char = '{' then LeftCurly
        else if char = '}' then RightCurly
        else if char = ':' then Keyword
        else if char = ';' then Comment
        else if char = '\\' then Character
        else if char = '\'' then Quote
        else if char = '`' then Quasiquote
        else if char = '~' then Unquote
        else if char = '"' then String
        else if char = '^' then Annotate
        else if char = '#' then UseTable
        else Atom
      in
      ((if token_type = Atom then state else state1), token_type)

  and run_token_untagged env state = function
    | RightParen | RightBracket | RightCurly -> Error (ErrorGroup state)
    | LeftParen -> parse_list RightParen list env state
    | LeftBracket -> parse_list RightBracket vec env state
    | LeftCurly -> parse_list RightCurly map env state
    | Keyword -> parse_prefix keyword env state
    | Quote -> parse_prefix quote env state
    | Quasiquote -> parse_prefix quasiquote env state
    | Unquote -> parse_prefix unquote env state
    | Comment -> parse_comment env state
    | String -> parse_string env state
    | Annotate -> parse_annotate env state
    | UseTable -> parse_with_table env state
    | Character -> Ok (parse_character state)
    | Atom -> Ok (parse_atom state)

  and run_token env state token_type =
    match run_token_untagged env state token_type with
    | Ok (state1, expr) -> Ok (state1, Source (expr, state.pos, state1.pos))
    | Error _ as e -> e

  and parse_with_table env state =
    let state, token_type = next_token state in
    match token_type with
    | Atom -> (
        match run_token env state token_type with
        | Error _ as e -> e
        | Ok (state, token) -> (
            match token with
            | Int i -> (
                match parse_num i state with
                | Some (state, num) -> Ok (state, num)
                | None -> Error (ExpectedNumber (state, i)))
            | Symbol s -> (
                match Base.Queue.find ~f:(fun (t, _) -> s = t) env with
                | None -> Error (NotFound (state, s))
                | Some (_, props) -> (
                    match List.find_opt (fun (p, _) -> p = Parser) props with
                    | None -> Error (NotFound (state, s))
                    | Some (_, parser) -> parser state))
            | _ -> Error (TableTypeError (state, token))))
    | Comment -> (
        match parse_toplevel env state with
        | Error _ as e -> e
        | Ok (state, comment) -> (
            match parse_toplevel env state with
            | Error _ as e -> e
            | Ok (state, expr) -> Ok (state, Comment (comment, expr))))
    | _ -> Error (TableError (state, token_type))

  and parse_annotate env state =
    match parse_toplevel env state with
    | Error _ as e -> e
    | Ok (state, annot) -> (
        match parse_toplevel env state with
        | Error _ as e -> e
        | Ok (state, data) -> Ok (state, Annotation (annot, data)))

  and parse_character state =
    let rec is_valid_char char = Uucp.Alpha.is_alphabetic char
    and parse_symbol_acc state acc =
      let state1, char = next_uchar state in
      if is_valid_char char then (
        Base.Queue.enqueue acc char;
        parse_symbol_acc state1 acc)
      else state
    and acc = Base.Queue.create () in
    let state1 = parse_symbol_acc state acc in
    let length = Base.Queue.length acc in
    let state, char =
      if length = 0 then next_uchar state
      else if length = 1 then (state1, Base.Queue.get acc 0)
      else
        let string = Buffer.create length in
        Base.Queue.iter acc ~f:(fun c -> Buffer.add_utf_8_uchar string c);
        ( state1,
          Uchar.of_char
            (match Buffer.contents string with
            | "alert" -> '\x07'
            | "apostrophe" -> '\''
            | "at-sign" -> '@'
            | "backspace" -> '\x08'
            | "backtick" -> '`'
            | "caret" -> '^'
            | "colon" -> ':'
            | "comma" -> ','
            | "delete" -> '\x7f'
            | "escape" -> '\x1b'
            | "form-feed" -> '\x0c'
            | "hash" -> '#'
            | "left-parenthesis" -> '('
            | "left-bracket" -> '['
            | "left-brace" -> '{'
            | "period" -> '.'
            | "newline" -> '\n'
            | "null" -> '\x00'
            | "quote" -> '"'
            | "return" -> '\r'
            | "right-parenthesis" -> ')'
            | "right-bracket" -> ']'
            | "right-brace" -> '}'
            | "semicolon" -> ';'
            | "space" -> ' '
            | "tab" -> '\t'
            | "tilde" -> '~'
            | _ -> raise Exit) )
    in
    (state, Char char)

  and parse_prefix form env state =
    match parse_toplevel env state with
    | Ok (state, expr) -> Ok (state, form expr)
    | Error _ as e -> e

  and parse_comment env state =
    let rec parse_line ({ input; pos } as state) buffer =
      let char = input.:[pos] in
      if char = '\n' then state
      else (
        Buffer.add_char buffer char;
        parse_line { input; pos = pos + 1 } buffer)
    in
    let buffer = Buffer.create 16 in
    let state = parse_line state buffer in
    match parse_toplevel env state with
    | Error _ as e -> e
    | Ok (state, expr) ->
        Ok (state, Comment (String (Buffer.contents buffer), expr))

  and parse_list term form env state =
    let rec parse_list_members state acc =
      let state1, token = next_token state in
      if token = term then Ok state1
      else
        match parse_toplevel env state with
        | Ok (state, expr) ->
            Base.Queue.enqueue acc expr;
            parse_list_members state acc
        | Error _ as e -> e
    in
    let acc = Base.Queue.create () in
    match parse_list_members state acc with
    | Ok state -> Ok (state, form (Base.Queue.to_list acc))
    | Error _ as e -> e

  and parse_toplevel env state =
    let state, token_type = next_token state in
    match run_token env state token_type with
    | Error _ as e -> e
    | Ok (state1, expr) -> (
        let rec loop state1 acc =
          let state2, char = next_uchar state1 in
          if char =? ':' then
            let state3, token_type = next_token state2 in
            match run_token env state3 token_type with
            | Error _ as e -> e
            | Ok (state4, expr) ->
                let result = Property (acc, expr) in
                loop state4 result
          else Ok (state1, acc)
        in
        match loop state1 expr with
        | Error _ as e -> e
        | Ok (state2, expr) ->
            let result = Source (expr, state.pos, state2.pos) in
            Ok (state2, result))

  let display_int result i = Buffer.add_string result (Int.to_string i)

  let display_float result f =
    if Float.is_nan f then Buffer.add_string result "#NaN"
    else if f = Float.infinity then Buffer.add_string result "#Inf"
    else if f = Float.neg_infinity then Buffer.add_string result "#-Inf"
    else Buffer.add_string result (Float.to_string f)

  let display_char result c =
    Buffer.add_char result '\\';
    Buffer.add_utf_8_uchar result c

  let display_substring result s =
    let fold c =
      if c = '"' || c = '\\' then Buffer.add_char result '\\';
      Buffer.add_char result c
    in
    String.iter fold s

  let display_string result s =
    Buffer.add_char result '"';
    display_substring result s;
    Buffer.add_char result '"'

  let rec display_to result = function
    | Int i -> display_int result i
    | Float f -> display_float result f
    | Char c -> display_char result c
    | Symbol s -> Buffer.add_string result s
    | String s -> display_string result s
    | Comment (comment, expr) -> display_comment result (comment, expr)
    | Template exprs -> display_template result exprs
    | Keyword expr -> display_prefix ":" result expr
    | Quote expr -> display_prefix "'" result expr
    | Quasiquote expr -> display_prefix "`" result expr
    | Unquote expr -> display_prefix "~" result expr
    | Source (expr, start, term) -> display_source result (expr, start, term)
    | Annotation (annot, expr) -> display_annotation result (annot, expr)
    | Property (parent, child) -> display_property result (parent, child)
    | List exprs -> display_series '(' ')' result exprs
    | Vec exprs -> display_series '[' ']' result exprs
    | Map exprs -> display_series '{' '}' result exprs

  and display_comment result (comment, expr) =
    Buffer.add_string result "#;";
    display_to result comment;
    Buffer.add_char result ' ';
    display_to result expr

  and display_source result (expr, start, term) =
    Buffer.add_string result "#;(at ";
    display_int result start;
    Buffer.add_char result ' ';
    display_int result term;
    Buffer.add_string result ") ";
    display_to result expr

  and display_annotation result (annot, expr) =
    Buffer.add_char result '^';
    display_to result annot;
    Buffer.add_char result ' ';
    display_to result expr

  and display_template result exprs =
    Buffer.add_char result '"';
    let fold = function
      | String s -> display_substring result s
      | Char c ->
          Buffer.add_char result '\\';
          Buffer.add_utf_8_uchar result c
      | expr ->
          Buffer.add_char result '\\';
          display_to result expr
    in
    List.iter fold exprs;
    Buffer.add_char result '"'

  and display_property result (parent, child) =
    display_to result parent;
    Buffer.add_char result ':';
    display_to result child

  and display_prefix init result expr =
    Buffer.add_string result init;
    display_to result expr

  and display_series init term result exprs =
    Buffer.add_char result init;
    let rec display_part = function
      | [] -> Buffer.add_char result term
      | [ elem ] ->
          display_to result elem;
          Buffer.add_char result term
      | elem :: rest ->
          display_to result elem;
          Buffer.add_char result ' ';
          display_part rest
    in
    display_part exprs

  let display expr =
    let buffer = Buffer.create 32 in
    display_to buffer expr;
    Buffer.contents buffer
end

module StringParser = Parser (struct
  type t = string

  let ( .:[] ) string i = string.[i]
  let ( .:<[] ) string _ = string
end)

module InChannelInput = struct
  type t = { input : In_channel.t; buffer : Buffer.t }

  let rec ( .:[] ) ({ input; buffer } as state) i =
    if i < Buffer.length buffer then Buffer.nth buffer i
    else (
      Buffer.add_channel buffer input (Buffer.length buffer - i + 1);
      state.:[i])

  let ( .:<[] ) input i =
    let _ = input.:[i] in
    Buffer.sub input.buffer 0 i
end

module InChannelParser = Parser (InChannelInput)
