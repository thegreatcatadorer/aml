let ( & ) = Int.logand
let ( $ ) = Int.logor
let ( ^ ) = Int.logxor
let ( << ) = Int.shift_left
let ( >> ) = Int.shift_right_logical
let select b p i = (i & b << p) >> p
let head i = i & 0xffff
let tail i = i >> 16
let first = select 0xff 16
let second = select 0xff 24
let third = select 0xff 32
(*
let fourth = select 0xff 40
let fifth = select 0xff 48
*)

let assert_in a i =
  let max = (1 << i) - 1 in
  assert (a == (a & max))

let new0 i =
  assert_in i 16;
  i

let new1 i a =
  assert_in a 8;
  new0 i $ (a << 16)

let new2 i a b =
  assert_in b 8;
  new1 i a $ (b << 24)

let new3 i a b c =
  assert_in c 8;
  new2 i a b $ (c << 32)
(*
let new4 i a b c d =
  assert_in d 8;
  new3 i a b c $ (c << 40)

let new5 i a b c d e =
  assert_in e 8;
  new4 i a b c d $ (c << 48)
*)
let exit ~size = new1 0 size
let enter ~size = new1 1 size
let jump ~instr = 2 $ (instr << 16)
let call ~size ~(instr : int) = (new1 3 size, instr)
let point ~dest = new1 4 dest
let follow ~reg = new1 5 reg
let extern ~func = 6 $ (func << 16)
let alloc ~dest ~reg ~size = new3 16 dest reg size
let free ~reg = new1 17 reg
let realloc ~dest ~reg ~size = new3 18 dest reg size
let modify ~dest ~reg = new2 19 dest reg
let get_index_r ~dest ~array ~index = new3 20 dest array index
let get_index_c ~dest ~array ~index = new3 21 dest array index
let set_index_r ~reg ~array ~index = new3 22 reg array index
let set_index_c ~reg ~array ~index = new3 23 reg array index
let short ~reg ~const = new1 32 reg $ (const << 24)
let long ~reg ~const = (new1 33 reg, const)
let switch ~reg = new1 48 reg
let switch_mod ~reg ~ceil = new2 49 reg ceil
let switch_else ~reg ~ceil = new2 50 reg ceil
let when_zero ~reg = new1 51 reg
let when_equal_r ~fst ~snd = new2 52 fst snd
let when_equal_c ~reg ~const = new2 53 reg const
let when_less_r ~fst ~snd = new2 54 fst snd
let when_less_c ~reg ~const = new2 55 reg const
let when_greater_r ~fst ~snd = new2 56 fst snd
let when_greater_c ~reg ~const = new2 57 reg const
let add_r ~dest ~fst ~snd = new3 64 dest fst snd
let add_c ~dest ~reg ~const = new3 65 dest reg const
let mul_r ~dest ~fst ~snd = new3 66 dest fst snd
let mul_c ~dest ~reg ~const = new3 67 dest reg const
let sub_rr ~dest ~fst ~snd = new3 68 dest fst snd
let sub_rc ~dest ~reg ~const = new3 69 dest reg const
let sub_cr ~dest ~const ~reg = new3 70 dest const reg
let div_rr ~dest ~fst ~snd = new3 71 dest fst snd
let div_rc ~dest ~reg ~const = new3 72 dest reg const
let div_cr ~dest ~const ~reg = new3 73 dest const reg
let rem_rr ~dest ~fst ~snd = new3 74 dest fst snd
let rem_rc ~dest ~reg ~const = new3 75 dest reg const
let rem_cr ~dest ~const ~reg = new3 76 dest const reg
let and_r ~dest ~fst ~snd = new3 80 dest fst snd
let and_c ~dest ~reg ~const = new3 81 dest reg const
let ior_r ~dest ~fst ~snd = new3 82 dest fst snd
let ior_c ~dest ~reg ~const = new3 83 dest reg const
let xor_r ~dest ~fst ~snd = new3 84 dest fst snd
let xor_c ~dest ~reg ~const = new3 85 dest reg const
let shift_rr ~dest ~fst ~snd = new3 86 dest fst snd
let shift_rc ~dest ~reg ~const = new3 87 dest reg const
let shift_cr ~dest ~const ~reg = new3 88 dest const reg
let not ~dest ~reg = new2 89 dest reg

type eval_success = EvalSuccess

let copy a b =
  let rec loop i =
    let i = i - 1 in
    if i >= 0 then (
      b.(i) <- a.(i);
      loop i)
  in
  loop (Int.min (Array.length a) (Array.length b))

let shift a b =
  if b < 0 then Int.shift_right_logical a (-b) else Int.shift_left a b

let eval ~(extern : (int * int array -> unit) array) ~(env : int array)
    ~(main : int) : unit =
  let stack = Array.make 65535 0 in
  let allocs = Hashtbl.create 0 in
  let rec run ip bp =
    let cmd = env.(ip) and ip = ip + 1 in
    let get r = stack.(bp + r) in
    let set r i = stack.(bp + r) <- i in
    let pred b = run (ip + if b then 0 else 1) bp in
    match head cmd with
    | 0 (* exit *) -> run ip (bp - first cmd)
    | 1 (* enter *) -> run ip (bp + first cmd)
    | 2 (* jump *) -> run (tail cmd) bp
    | 3 (* call *) -> run env.(ip) (bp + first cmd)
    | 4 (* point *) ->
        set (first cmd) ip;
        run ip bp
    | 5 (* follow *) -> run (get (first cmd)) bp
    | 6 (* extern *) ->
        extern.(tail cmd) (bp, stack);
        run ip bp
    | 16 (* alloc *) ->
        let i = Hashtbl.length allocs in
        let l = get (second cmd) * third cmd in
        Hashtbl.add allocs i (Array.make l 0);
        set (first cmd) i;
        run ip bp
    | 17 (* free *) ->
        Hashtbl.remove allocs (get (first cmd));
        run ip bp
    | 18 (* realloc *) ->
        let p = get (first cmd) in
        let t = Hashtbl.find allocs p in
        let t1 = Array.make (get (second cmd) * third cmd) 0 in
        copy t t1;
        Hashtbl.replace allocs p t1;
        run ip bp
    | 19 (* get_index_r *) ->
        let t = Hashtbl.find allocs (get (second cmd)) in
        set (first cmd) t.(get (third cmd));
        run ip bp
    | 20 (* get_index_c*) ->
        let t = Hashtbl.find allocs (get (second cmd)) in
        set (first cmd) t.(third cmd);
        run ip bp
    | 21 (* set_index_r *) ->
        let t = Hashtbl.find allocs (get (second cmd)) in
        t.(get (third cmd)) <- get (first cmd);
        run ip bp
    | 22 (* set_index_c *) ->
        let t = Hashtbl.find allocs (get (second cmd)) in
        t.(third cmd) <- get (first cmd);
        run ip bp
    | 32 (* short *) ->
        set (first cmd) (tail cmd);
        run ip bp
    | 33 (* long *) ->
        set (first cmd) env.(ip);
        run (ip + 1) bp
    | 48 (* switch *) -> run (ip + get (first cmd)) bp
    | 49 (* switch_mod *) ->
        let d = Int.rem (get (first cmd)) (second cmd) in
        run (ip + d) bp
    | 50 (* switch_else *) ->
        let d = Int.min (get (first cmd)) (second cmd) in
        run (ip + d) bp
    | 51 (* when_zero *) -> pred (get (first cmd) == 0)
    | 52 (* when_equal_r *) -> pred (get (first cmd) == get (second cmd))
    | 53 (* when_equal_c *) -> pred (get (first cmd) == second cmd)
    | 54 (* when_less_r *) -> pred (get (first cmd) < get (second cmd))
    | 55 (* when_less_c *) -> pred (get (first cmd) < second cmd)
    | 56 (* when_greater_r *) -> pred (get (first cmd) > get (second cmd))
    | 57 (* when_greater_c *) -> pred (get (first cmd) > second cmd)
    | 64 (* add_r *) ->
        set (first cmd) (get (second cmd) + get (third cmd));
        run ip bp
    | 65 (* add_c *) ->
        set (first cmd) (get (second cmd) + third cmd);
        run ip bp
    | 66 (* mul_r *) ->
        set (first cmd) (get (second cmd) * get (third cmd));
        run ip bp
    | 67 (* mul_c *) ->
        set (first cmd) (get (second cmd) * third cmd);
        run ip bp
    | 68 (* sub_rr *) ->
        set (first cmd) (get (second cmd) - get (third cmd));
        run ip bp
    | 69 (* sub_rc *) ->
        set (first cmd) (get (second cmd) - third cmd);
        run ip bp
    | 70 (* sub_cr *) ->
        set (first cmd) (second cmd - get (third cmd));
        run ip bp
    | 71 (* div_rr *) ->
        set (first cmd) (get (second cmd) / get (third cmd));
        run ip bp
    | 72 (* div_rc *) ->
        set (first cmd) (get (second cmd) / third cmd);
        run ip bp
    | 73 (* div_cr *) ->
        set (first cmd) (second cmd / get (third cmd));
        run ip bp
    | 74 (* rem_rr *) ->
        set (first cmd) (Int.rem (get (second cmd)) (get (third cmd)));
        run ip bp
    | 75 (* rem_rc *) ->
        set (first cmd) (Int.rem (get (second cmd)) (third cmd));
        run ip bp
    | 76 (* rem_cr *) ->
        set (first cmd) (Int.rem (second cmd) (get (third cmd)));
        run ip bp
    | 80 (* and_r *) ->
        set (first cmd) (get (second cmd) & get (third cmd));
        run ip bp
    | 81 (* and_c *) ->
        set (first cmd) (get (second cmd) & third cmd);
        run ip bp
    | 82 (* ior_r *) ->
        set (first cmd) (get (second cmd) $ get (third cmd));
        run ip bp
    | 83 (* ior_c *) ->
        set (first cmd) (get (second cmd) $ third cmd);
        run ip bp
    | 84 (* xor_r *) ->
        set (first cmd) (get (second cmd) ^ get (third cmd));
        run ip bp
    | 85 (* xor_c *) ->
        set (first cmd) (get (second cmd) ^ third cmd);
        run ip bp
    | 86 (* shift_rr *) ->
        set (first cmd) (shift (get (second cmd)) (get (third cmd)));
        run ip bp
    | 87 (* shift_rc *) ->
        set (first cmd) (shift (get (second cmd)) (third cmd));
        run ip bp
    | 88 (* shift_cr *) ->
        set (first cmd) (shift (second cmd) (get (third cmd)));
        run ip bp
    | 89 (* not *) ->
        set (first cmd) (Int.lognot (get (second cmd)));
        run ip bp
    | _ -> EvalSuccess
  in
  let (_ : eval_success) = run main 0 in
  ()
