type token_type =
  | LeftParen
  | RightParen
  | LeftBracket
  | RightBracket
  | LeftCurly
  | RightCurly
  | Atom
  | Keyword
  | Comment
  | Character
  | Quote
  | Quasiquote
  | Unquote
  | String
  | Annotate
  | UseTable

type property = Parser

type expression =
  | Int of int
  | Float of float
  | Char of Uchar.t
  | String of string
  | Template of expression list
  | Symbol of string
  | Keyword of expression
  | Quote of expression
  | Quasiquote of expression
  | Unquote of expression
  | Source of expression * int * int
  | Annotation of expression * expression
  | Comment of expression * expression
  | Property of expression * expression
  | List of expression list
  | Vec of expression list
  | Map of expression list

type 'a error =
  | ErrorGroup of 'a
  | ExpectedNumber of 'a * int
  | TableError of 'a * token_type
  | TableTypeError of 'a * expression
  | NotFound of 'a * string
  | Unimplemented

module type input = sig
  type t

  val ( .:[] ) : t -> int -> char
  val ( .:<[] ) : t -> int -> string
end

module Parser (Input : input) : sig
  type state = { input : Input.t; pos : int }
  type parser = state -> (state * expression, state error) result
  type env = (string * (property * parser) list) Base.Queue.t

  val next_uchar : state -> state * Uchar.t
  val parse_toplevel : env -> parser
  val display : expression -> string
  val display_to : Buffer.t -> expression -> unit
end

module StringParser : sig
  type state = { input : string; pos : int }
  type parser = state -> (state * expression, state error) result
  type env = (string * (property * parser) list) Base.Queue.t

  val next_uchar : state -> state * Uchar.t
  val parse_toplevel : env -> parser
  val display : expression -> string
  val display_to : Buffer.t -> expression -> unit
end

module InChannelInput : sig
  type t = { input : In_channel.t; buffer : Buffer.t }
end

module InChannelParser : sig
  type state = { input : InChannelInput.t; pos : int }
  type parser = state -> (state * expression, state error) result
  type env = (string * (property * parser) list) Base.Queue.t

  val next_uchar : state -> state * Uchar.t
  val parse_toplevel : env -> parser
  val display : expression -> string
  val display_to : Buffer.t -> expression -> unit
end
