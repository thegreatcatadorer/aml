type expression = Expression of datum * metadata list

and datum =
  | Int of int
  | Float of float
  | Char of Uchar.t
  | String of string
  | Symbol of string
  | List of expression list

and metadata =
  | Range of int * int
  | Annotation of expression
  | Comment of expression

val of_parse_tree : metadata list -> Parser.expression -> expression