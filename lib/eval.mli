val eval :
  extern:(int * int array -> unit) array -> env:int array -> main:int -> unit

val exit : size:int -> int
val enter : size:int -> int
val jump : instr:int -> int
val call : size:int -> instr:int -> int * int
val point : dest:int -> int
val follow : reg:int -> int
val extern : func:int -> int
val alloc : dest:int -> reg:int -> size:int -> int
val free : reg:int -> int
val realloc : dest:int -> reg:int -> size:int -> int
val modify : dest:int -> reg:int -> int
val get_index_r : dest:int -> array:int -> index:int -> int
val get_index_c : dest:int -> array:int -> index:int -> int
val set_index_r : reg:int -> array:int -> index:int -> int
val set_index_c : reg:int -> array:int -> index:int -> int
val short : reg:int -> const:int -> int 
val long : reg:int -> const:int -> int * int
val switch : reg:int -> int
val switch_mod : reg:int -> ceil:int -> int
val switch_else : reg:int -> ceil:int -> int
val when_zero : reg:int -> int
val when_equal_r : fst:int -> snd:int -> int
val when_equal_c : reg:int -> const:int -> int
val when_less_r : fst:int -> snd:int -> int
val when_less_c : reg:int -> const:int -> int
val when_greater_r : fst:int -> snd:int -> int
val when_greater_c : reg:int -> const:int -> int
val add_r : dest:int -> fst:int -> snd:int -> int
val add_c : dest:int -> reg:int -> const:int -> int
val mul_r : dest:int -> fst:int -> snd:int -> int
val mul_c : dest:int -> reg:int -> const:int -> int
val sub_rr : dest:int -> fst:int -> snd:int -> int
val sub_rc : dest:int -> reg:int -> const:int -> int
val sub_cr : dest:int -> const:int -> reg:int -> int
val div_rr : dest:int -> fst:int -> snd:int -> int
val div_rc : dest:int -> reg:int -> const:int -> int
val div_cr : dest:int -> const:int -> reg:int -> int
val rem_rr : dest:int -> fst:int -> snd:int -> int
val rem_rc : dest:int -> reg:int -> const:int -> int
val rem_cr : dest:int -> const:int -> reg:int -> int
val and_r : dest:int -> fst:int -> snd:int -> int
val and_c : dest:int -> reg:int -> const:int -> int
val ior_r : dest:int -> fst:int -> snd:int -> int
val ior_c : dest:int -> reg:int -> const:int -> int
val xor_r : dest:int -> fst:int -> snd:int -> int
val xor_c : dest:int -> reg:int -> const:int -> int
val shift_rr : dest:int -> fst:int -> snd:int -> int
val shift_rc : dest:int -> reg:int -> const:int -> int
val shift_cr : dest:int -> const:int -> reg:int -> int
val not : dest:int -> reg:int -> int

(* Instruction set:
   Function calls:
     exit const: bp -= $1
     enter const: bp += $1
     jump & const: ip = $&
     call const, & const: bp += $1; ip = $&
     point reg: $1 = ip
     follow reg: ip = $1
     extern & const: ext[$&](bp, stack)
   Pointers and allocation:
     alloc reg reg const: $1 = calloc($2 * $3)
     free reg: free($1)
     realloc reg reg const: $_1 = calloc($2 * $3); memcpy($1, $_1); $1 = $_1
     modify reg reg: $1 = $2
     get_index_r reg ptr reg: $1 = $2[$3]
     get_index_c reg ptr const: $1 = $2[$3]
     set_index_r reg ptr reg: $2[$3] = $1
     set_index_c reg ptr const: $2[$3] = $1
   Constants:
     short reg & const: $1 = $&
     long reg, & const: $1 = $&
   Conditionals:
     switch reg: ip += $1
     switch_mod reg const: ip += ($1 % $2)
     switch_else reg const: ip += min($1, $2)
     when_zero reg: if ($1 == 0) ip += 2 else ip += 1
     when_equal_r reg reg: if ($1 == $2) ip += 2 else ip += 1
     when_equal_c reg const: if ($1 == $2) ip += 2 else ip += 1
     when_less_r reg reg: if ($1 < $2) ip += 2 else ip += 1
     when_less_c reg const: if ($1 < $2) ip += 2 else ip += 1
     when_greater_r reg reg: if ($1 > $2) ip += 2 else ip += 1
     when_greater_c reg const: if ($1 > $2) ip += 2 else ip += 1
   Arithmetic:
     add_r reg reg reg: $1 = $2 + $3
     add_c reg reg const: $1 = $2 + $3
     mul_r reg reg reg: $1 = $2 * $3
     mul_c reg reg const: $1 = $2 * $3
     sub_rr reg reg reg: $1 = $2 - $3
     sub_rc reg reg const: $1 = $2 - $3
     sub_cr reg const reg: $1 = $2 - $3
     div_rr reg reg reg: $1 = $2 / $3
     div_rc reg reg const: $1 = $2 / $3
     div_cr reg const reg: $1 = $2 / $3
     rem_rr reg reg reg: $1 = $2 % $3
     rem_rc reg reg const: $1 = $2 % $3
     rem_cr reg const reg: $1 = $2 % $3
   Binary:
     and_r reg reg reg: $1 = $2 & $3
     and_c reg reg const: $1 = $2 & $3
     ior_r reg reg reg: $1 = $2 | $3
     ior_c reg reg const: $1 = $2 | $3
     xor_r reg reg reg: $1 = $2 ^ $3
     xor_c reg reg const: $1 = $2 ^ $3
     shift_rr reg reg reg: $1 = $2 << $3
     shift_rc reg reg const: $1 = $2 << $3
     shift_cr reg const reg: $1 = $2 << $3
     not reg reg: $1 = ~$2
*)
