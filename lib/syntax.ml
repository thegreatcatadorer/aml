type expression = Expression of datum * metadata list

and datum =
  | Int of int
  | Float of float
  | Char of Uchar.t
  | String of string
  | Symbol of string
  | List of expression list

and metadata =
  | Range of int * int
  | Annotation of expression
  | Comment of expression

let sym s = Expression (Symbol s, [])

let rec of_parse_tree acc = function
  | Parser.Int i -> Expression (Int i, acc)
  | Parser.Float f -> Expression (Float f, acc)
  | Parser.Char c -> Expression (Char c, acc)
  | Parser.String s -> Expression (String s, acc)
  | Parser.Template s ->
      Expression (List (sym "string" :: List.map (of_parse_tree []) s), acc)
  | Parser.Symbol s -> Expression (Symbol s, acc)
  | Parser.Keyword s ->
      Expression (List [ sym "keyword"; of_parse_tree [] s ], acc)
  | Parser.Quote s -> Expression (List [ sym "quote"; of_parse_tree [] s ], acc)
  | Parser.Quasiquote s ->
      Expression (List [ sym "quasiquote"; of_parse_tree [] s ], acc)
  | Parser.Unquote s ->
      Expression (List [ sym "unquote"; of_parse_tree [] s ], acc)
  | Parser.Property (parent, child) ->
      Expression (List [ of_parse_tree [] parent; of_parse_tree [] child ], acc)
  | Parser.Source (expr, init, term) ->
      of_parse_tree (Range (init, term) :: acc) expr
  | Parser.Annotation (annot, expr) ->
      of_parse_tree (Annotation (of_parse_tree [] annot) :: acc) expr
  | Parser.Comment (comment, expr) ->
      of_parse_tree (Comment (of_parse_tree [] comment) :: acc) expr
  | Parser.List exprs ->
      let result = List.map (of_parse_tree []) exprs in
      Expression (List result, acc)
  | Parser.Vec exprs ->
      let result = sym "vec" :: List.map (of_parse_tree []) exprs in
      Expression (List result, acc)
  | Parser.Map exprs ->
      let result = sym "map" :: List.map (of_parse_tree []) exprs in
      Expression (List result, acc)
