type variable = Var of int
type label = Label of int
type size = Size64

type intrinsic =
  | Imm
  | Add
  | Sub
  | Mul
  | Div
  | And
  | Or
  | Xor
  | Shl
  | Shr
  | Rotl
  | Rotr
  | Ord
  | Asc
  | Load
  | Store
  | Call

type expression = Expression of intrinsic * variable list

type statement =
  | Label of label
  | Assignment of variable * expression
  | Branch of label * expression

type func = Func of size Array.t * statement Array.t
