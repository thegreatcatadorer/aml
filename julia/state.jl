# Creates a series of states that can be jumped between
# Uses the while true; case ... idiom
# `continue!(state)`` will interrupt the current action, resuming with `state`
# Unsafe to nest due to not using structured control flow
# Does not have fallthrough; not jumping in an expression will return its value

function compilemachine(expr::Expr)
    @gensym state result
    states = args(expr)
    nameof(expr) = (@assert(expr.head == :(=)); args(expr)[1])
    count = Dict(zip(nameof.(states), 1:length(states)))
    function rewrite(expr, currentstate)
        isa(expr, Expr) || return expr
        recur(expr) = rewrite(expr, currentstate)
        if expr.head == :call && args(expr)[1] == :continue!
            dest = args(expr)[2] == :this ? currentstate : args(expr)[2]
            setstate = Expr(:(=), state, count[dest])
            changestate = Expr(:continue)
            return Expr(:block, setstate, changestate)
        end
        Expr(expr.head, recur.(args(expr))...)
    end
    body = Expr(:break)
    for decl in reverse(states)
        name, subblock = args(decl)
        test = Expr(:call, :isequal, state, count[name])
        block = Expr(:block, Expr(:(=), result, rewrite(subblock, name)), Expr(:break))
        body = Expr(:if, test, block, body)
    end
    cases = Expr(:while, :(true), body)
    inits = [Expr(:local, Expr(:(=), state, 1)), Expr(:local, result)]
    Expr(:block, inits..., cases, result)
end

macro state(body)
    esc(compilemachine(body))
end
