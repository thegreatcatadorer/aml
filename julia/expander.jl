import Base.iterate, Base.convert

@data Ast begin
    AstInt(Int)
    AstFloat(Float64)
    AstSymbol(String)
    AstString(String)
    AstList(Vector{Ast})
end

@data CoreIR begin
    CoreDo(Vector{CoreIR})
    CoreDef(CoreSymbol, CoreIR)
    CoreDecl(CoreSymbol, CoreIR)
    CoreInvoke(CoreSymbol, Vector{CoreIR})
    CoreIf(CoreIR, CoreIR, CoreIR)
    CoreFn(CoreIR, CoreIR, CoreIR)
    CoreInt(Int)
    CoreFloat(Float64)
    CoreSymbol(String)
    CoreString(String)
    CoreResult(Vector{Tuple{CoreSymbol,CoreIR}})
    CoreRecord(Vector{Tuple{CoreSymbol,CoreIR}})
end

macro sym_str(string)
    Expr(:call, :AstString, string)
end

astcall(func, args...) = AstList(AstSymbol(func), args...)
astquote(ast) = astcall("quote", ast)
astkeyword(ast) = astcall("keyword", ast)

Base.iterate(list::List) = iterate(list, list)
Base.iterate(::List, ::CstNull) = nothing
Base.iterate(::List, list::CstCons) = list._1, list._2

Base.collect(list::List) = [list...]

Base.convert(::Type{Ast}, cst::Cst)::Ast = @match cst begin
    CstInt(i) => AstInt(i)
    CstFloat(n) => AstFloat(n)
    CstSymbol(a) => AstSymbol(a)
    CstString(a) => AstString(a)
    t::Token => throw(ErrorException("unparsed token in ast ($t)!"))
    list::Union{CstCons,CstNull} => AstList([list...])
end

Base.convert(::Type{Cst}, ast::Ast)::Cst = @match ast begin
    AstInt(i) => CstInt(i)
    AstFloat(n) => CstFloat(n)
    AstSymbol(a) => CstSymbol(a)
    AstString(a) => CstString(a)
    AstList(list) => cstlist(list...)
end

# Design notes:
# I'd like to have a system where macros are subsituted with function-forms
# that are then evaluated on their arguments - it's a more general mechanism.
# However, that violates phase-separation, which means I can't use it until
# the system is near-complete. Instead, we call Julia functions on the syntax
# they are embedded in - it's a lot easier to write in Julia.

const macrotable = Dict{String,Function}()

function expandtoplevel(ast, data)
    if ast isa AstList &&
       length(ast._1) != 0 &&
       (head = ast._1[1]) isa AstSymbol &&
       haskey(macrotable, headstr)
        macrotable[headstr](ast, data)
    else
        nothing
    end
end

function expand(ast, data)
    recur(ast) = ((result, data=expand(ast, data)); result)
    while (update = expandtoplevel(ast, data)) !== nothing
        ast, data = update
    end
    AstList(recur.(ast._1)), data
end

function declfn(name, args, returntype)
    fntype = astcall("Fn", args, returntype)
    fntype = astcall("internal/type-context", fntype)
    astcall("internal/decl-method", name, fntype)
end

macrotable["decl"] = function expanddecl(ast, _)
    _, val, valtype = ast._1
    valtype = astcall("internal/type-context", valtype)
    astcall("internal/decl", val, valtype)
end

macrotable["defn"] = function expanddefn(ast, _)
    _, name, args, returntype, body... = ast._1
    fntype = declfn(name, args, returntype)
    func = astcall("fn", args, returntype, body...)
    func = astcall("internal/add-method!", name, func)
    astcall("do", fntype, func)
end

macrotable["declfn"] = function expanddeclfn(ast, _)
    _, name, args, returntype = ast._1
    declfn(name, args, returntype)
end

function parsecore(ast::Ast, data)
    ast = expand(ast, Dict())[1]
    @match ast begin
        AstInt(i) => CoreInt(i)
        AstFloat(n) => CoreFloat(n)
        AstSymbol(a) => CoreSymbol(a)
        AstString(a) => CoreString(a)
        AstList(head, args...) => parseapply(head, args, data)
    end
end

function parseapply(head::AstSymbol, args, data)
    recur(ast) = parsecore(ast, data)
    headstr, coreargs = head._1, recur.(args)
    if headstr == "do"
        CoreDo(coreargs)
    elseif headstr == "def"
        name, value = coreargs
        CoreDef(name, value)
    elseif headstr == "decl"
        name, type = coreargs
        CoreDecl(name, type)
    elseif headstr == "if"
        cond, then, otherwise = coreargs
        CoreIf(cond, then, otherwise)
    elseif headstr == "fn"
        fnargs, result, body... = coreargs
        CoreFn(fnargs, result, CoreDo(parseapply.(body)))
    elseif headstr == "literal"
        @assert length(args) == 1
        expandliteral(args[1])
    elseif headstr == "Result"
        args1, cases = coreargs, Tuple{CoreSymbol,CoreIR}[]
        while !isempty(args1)
            push!(cases, (popfirst!(args1), popfirst!(args1)))
        end
        CoreResult(cases)
    elseif headstr == "Record"
        args1, cases = coreargs, Tuple{CoreSymbol,CoreIR}[]
        while !isempty(args1)
            push!(cases, (popfirst!(args1), popfirst!(args1)))
        end
        CoreRecord(cases)
    elseif (func = findfunction(headstr, data)) !== nothing
        CoreInvoke(func, coreargs)
    else throw(ErrorException("invalid core: call of $head on $args"))
    end
end

function corelist(exprs)
    result = CoreIR[]
    resultname = CoreSymbol(uniquename("list-result"))
    newlist = CoreInvoke(CoreSymbol("new-list"), [])
    push!(result, CoreDef(resultname, newlist))
    for e in exprs
        push!(result, CoreInvoke(CoreSymbol("conj!"), [resultname, e]))
    end
    CoreDo(result)
end

function expandliteral(expr)
    @match expr begin
        AstInt(i) => CoreInt(i)
        AstFloat(n) => CoreFloat(n)
        AstString(a) => CoreString(a)
        AstSymbol(a) => CoreInvoke(CoreSymbol("symbol"), [CoreString(a)])
        AstList(exprs) => corelist(expandlitera.(exprs))
    end
end