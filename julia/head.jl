module Aml
import Pkg
using Test

Pkg.add("MLStyle")
using MLStyle

include("utils.jl")
include("map.jl")
include("state.jl")
include("consumer.jl")
include("parser.jl")
include("expander.jl")
include("eval.jl")

end
