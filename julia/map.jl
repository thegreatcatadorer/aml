abstract type Map{K,V,H} <: AbstractDict{K,V} end

const D::UInt = 5
withbit(n) = unsigned(1) << n
shiftout(n) = (n & (1 << D - 1), n >>> D)
indexfor(n, occupied) = count_ones((withbit(n) - 1) & occupied) + 1

struct MapEmpty{K,V,H} <: Map{K,V,H} end

struct MapLeaf{K,V,H} <: Map{K,V,H}
    remainder::UInt
    value::V
end

struct MapBranch{K,V,H} <: Map{K,V,H}
    occupied::UInt
    members::Vector{Map{K,V,H}}
end

search(key::K, map::Map{K,V,H}) where {K,V,H} = search(H(key), map)
search(::UInt, ::MapEmpty) = nothing
search(key::UInt, map::MapLeaf) = key == map.remainder ? map.value : nothing
search(key::UInt, map::MapBranch{K,V,H}) where {K,V,H} = begin
    low, high = shiftout(key)
    (withbit(low) & map.occupied) == 0 && return nothing
    search(high, map.members[indexfor(low, map.occupied)])
end
Base.getindex(map::Map, key) = search(key, map)

function modify(key, value, map::Map{K,V,H}) where {K,V,H}
    modify(H(key), value, map)
end
function modify(key::UInt, value, ::MapEmpty{K,V,H}) where {K,V,H}
    MapLeaf{K,V,H}(key, value)
end
function modify(key::UInt, value, map::MapLeaf{K,V,H}) where {K,V,H}
    key == map.remainder && return MapLeaf{K,V,H}(key, value)
    oldlow, oldhigh = shiftout(map.remainder)
    newlow, newhigh = shiftout(key)
    oldleaf = MapLeaf{K,V,H}(oldhigh, map.value)
    members = if oldlow == newlow
        Map{K,V,H}[modify(newhigh, value, oldleaf)]
    else
        newleaf = MapLeaf{K,V,H}(newhigh, value)
        if oldlow > newlow
            Map{K,V,H}[newleaf, oldleaf]
        else
            Map{K,V,H}[oldleaf, newleaf]
        end
    end
    MapBranch{K,V,H}(withbit(oldlow) | withbit(newlow), members)
end
function modify(key::UInt, value, map::MapBranch{K,V,H}) where {K,V,H}
    low, high = shiftout(key)
    members, occupied = copy(map.members), map.occupied
    index = indexfor(low, occupied)
    if (withbit(low) & occupied) != 0
        members[index] = modify(high, value, members[index])
    else
        occupied |= withbit(low)
        insert!(members, index, MapLeaf{K,V,H}(high, value))
    end
    MapBranch{K,V,H}(occupied, members)
end

function frompairs(::Type{Map}, args::Vararg{Pair{K,V}}; H::Function = hash) where {K,V}
    map = MapEmpty{K,V,H}()
    for pair in args
        map = modify(pair..., map)
    end
    map
end

remove(key, map::Map{K,V,H}) where {K,V,H} = remove(H(key), map)
remove(::UInt, map::MapEmpty{K,V,H}) where {K,V,H} = map
remove(key::UInt, map::MapLeaf{K,V,H}) where {K,V,H} =
    key == map.remainder ? MapEmpty{K,V,H}() : map
function remove(key::UInt, map::MapBranch{K,V,H}) where {K,V,H}
    low, high = shiftout(key)
    (withbit(low) & map.occupied) == 0 && return map
    index = indexfor(low, map.occupied)
    child = map.members[index]
    update = remove(high, child)
    child === update && return map
    if child isa MapEmpty
        xor(map.occupied, withbit(low)) == 0 && return MapEmpty{K,V,H}()
        members = copy(map.members)
        popat!(members, index)
        MapBranch{K,V,H}(xor(map.occupied, withbit(low)), members)
    else
        members = copy(map.members)
        members[index] = child
        MapBranch{K,V,H}(map.occupied, members)
    end
end

Base.collect(::MapEmpty) = []
Base.collect(map::MapLeaf) = [map.value]
Base.collect(map::MapBranch) = vcat(collect.(map.members)...)

function Base.iterate(map::Map)
    values = collect(map)
    iterate(map, (values, firstindex(values)))
end
function Base.iterate(::Map, (vec, index))
    (result = iterate(vec, index)) === nothing && return nothing
    next, rest = result
    next, (vec, rest)
end

Base.show(io::IO, ::MIME"text/plain", map::Map) = show(io, map)
function Base.show(io::IO, ::MapEmpty{K,V,H}) where {K,V,H}
    print(io, "MapEmpty{", K, ", ", V, ", ", H, "}()")
end
function Base.show(io::IO, map::MapLeaf{K,V,H}) where {K,V,H}
    print(io, "MapLeaf{", K, ", ", V, ", ", H, "}(", map.remainder, ", ", map.value, ")")
end
function Base.show(io::IO, map::MapBranch{K,V,H}) where {K,V,H}
    print(io, "MapBranch{", K, ", ", V, ", ", H, "}(", map.occupied, ", ", map.members, ")")
end
