abstract type Cst end

# We probably shouldn't be doing this; it causes worse error messages when
# we have to convert it into an AST and we've lost the position information.
@data Token <: Cst begin
    Access
    OpenList
    OpenMap
    OpenVec
    CloseList
    CloseMap
    CloseVec
    OpenString
    Comment
    Character
    Invoke
    Keyword
    Quasiquote
    Quote
    Unquote
end

@data List <: Cst begin
    CstNull()
    CstCons(Cst, List)
end

@data Value <: Cst begin
    CstInt(Int)
    CstFloat(Float64)
    CstSymbol(String)
    CstString(String)
end

runparser(string) = expr(string, firstindex(string))

function macrochar(char)
    char == '(' ? OpenList :
    char == '{' ? OpenMap :
    char == '[' ? OpenVec :
    char == ')' ? CloseList :
    char == '}' ? CloseMap :
    char == ']' ? CloseVec :
    char == '"' ? OpenString :
    char == ':' ? Keyword :
    char == '\'' ? Quote :
    char == '`' ? Quasiquote :
    char == '~' ? Unquote :
    char == '#' ? Invoke :
    char == ';' ? Comment :
    char == '\\' ? Character : nothing
end

function token(obj, state)
    char, nextstate = iterate(obj, state)
    while isspace(char) || char == ','
        state = nextstate
        char, nextstate = iterate(obj, nextstate)
    end
    (action = macrochar(char)) !== nothing && return action, nextstate
    (char in '0':'9' || char == '-') && (num=parsenumber(obj, state))[1] !== nothing && return num
    parsesymbol(obj, state)
end

@fold error(message::String) = throw(ErrorException("at $state, in $obj: $message"))
@fold function group()
    let token = next!(token)
        token == Comment ? (next!(parsecomment); next!(expr)) :
        token == OpenList ? next!(list, "list", CloseList) :
        token == OpenMap ? CstCons(CstSymbol("syntax/map"), next!(list, "map", CloseMap)) :
        token == OpenVec ? CstCons(CstSymbol("syntax/vec"), next!(list, "vec", CloseVec)) :
        token == OpenString ? next!(parsestring) :
        token == Character ? next!(parsecharacter) :
        token == Keyword ? cstlist(CstSymbol("syntax/keyword"), next!(expr)) :
        token == Quote ? cstlist(CstSymbol("syntax/quote"), next!(expr)) :
        token == Quasiquote ? cstlist(CstSymbol("syntax/quasiquote"), next!(expr)) :
        token == Unquote ? cstlist(CstSymbol("syntax/unquote"), next!(expr)) :
        token == Invoke ? next!(invokefunc) : token
    end
end

@fold function expr()
    result = next!(group)
    while peek!() == '.'
        commit!()
        result = cstlist(next!(group), result)
    end
    result
end

@fold function list(name, token)
    let result = []
        while true
            if (next = next!(expr)) |> isendtoken
                next == token && break
                next!(error, "$name closed with $next")
            else
                push!(result, next)
            end
        end
        cstlist(result...)
    end
end

isendtoken(obj) = obj == CloseList || obj == CloseMap || obj == CloseVec

function cstlist(args...)
    result::List = CstNull()
    for a in reverse(args)
        result = CstCons(a, result)
    end
    result
end



@fold parsenumber(base=10) = begin
    isnumeric, isnegated = false, false
    integer, decimal, exponent = [], [], []
    case = 0
    while true
        if case == 0
            peek!() == '-' && (commit!(); isnegated = true)
            case = 1
        elseif case == 1
            c = peek!()
            if isnumber(c, base)
                commit!()
                isnumeric = true
                push!(integer, c)
            elseif c == '_'
                commit!()
            elseif c == '.'
                commit!()
                case = 2
            elseif c == 'e' || c == 'E'
                commit!()
                case = 3
            else
                break
            end
        elseif case == 2
            c = peek!()
            if isnumber(c, base)
                commit!()
                isnumeric = true
                push!(decimal, c)
            elseif c == '_'
                commit!()
            elseif c == 'e' || c == 'E'
                commit!()
                case = 3
            else
                break
            end
        elseif case == 3
            c = peek!()
            if isnumber(c, base)
                commit!()
                isnumeric = true
                push!(exponent, c)
            elseif c == '_'
                commit!()
            else
                break
            end
        else
            break
        end
    end
    isnumeric || return nothing
    if isempty(decimal) && isempty(exponent)
        result = numberof(integer, base)
        isnegated && (result *= -1)
        CstInt(result)
    else
        result =
            (numberof(integer, base) + decimalof(decimal, base)) * base^numberof(exponent, base)
        isnegated && (result *= -1.0)
        CstFloat(result)
    end
end

@fold function parsecomment()
    while (char = peek!()) != '\n'
        commit!()
    end
    Comment
end

@fold function parsestring()
    contents, case = Char[], 1
    while true
        if case == 1
            c = next!()
            if c == '"'
                break
            elseif c == '\\'
                case = 2
            else
                push!(contents, c)
            end
        elseif case == 2
            c = next!()
            if c == '\\' || c == '"' || c == '\''
                push!(contents, c)
            elseif c == 'e'
                push!(contents, '\e')
            elseif c == 'n'
                push!(contents, '\n')
            elseif c == 'r'
                push!(contents, '\r')
            elseif c == 't'
                push!(contents, '\t')
            elseif c == 'x'
                num = 0x10 * number(next!(), 16) + number(next!(), 16)
                push!(contents, Char(num))
            elseif c == 'u'
                num = 0x1000 * number(next!(), 16) + 0x100 * number(next!(), 16)
                num += 0x10 * number(next!(), 16) + number(next!(), 16)
                push!(contents, Char(num))
            elseif c == 'U'
                num = 0x100000 * number(next!(), 16) + 0x10000 * number(next!(), 16)
                num += 0x1000 * number(next!(), 16) + 0x100 * number(next!(), 16)
                num += 0x10 * number(next!(), 16) + number(next!(), 16)
                push!(contents, Char(num))
            elseif c == '('
                next!(error, "list formatting not implemented yet")
            else
                push!(contents, '\\', c)
            end
            case = 1
        else
            break
        end
    end
    CstString(String(contents))
end

@fold function parsecharacter()
    if 'a' <= (result = peek!()) <= 'z'
        namechars = Char[]
        while 'a' <= (next = peek!) <= 'z'
            commit!()
            push!(namechars, next)
        end
        name = String(namechars)
        if name == "space"
            result = ' '
        elseif name == "tab"
            result = '\t'
        elseif name == "newline"
            result = '\n'
        elseif name == "return"
            result = '\r'
        elseif name == "formfeed"
            result = '\f'
        elseif name == "backspace"
            result = '\b'
        elseif name == "escape"
            result = '\e'
        elseif name == "alert"
            result = '\a'
        elseif length(name) != 1
            next!(error, "unknown character: '$name'")
        else
            result = name[1]
        end
    else
        commit!()
    end
    CstInt(Int(result))
end

@fold function parsesymbol()
    word = Char[]
    while true
        c = peek!()
        if c in ".~@#^{}[]();,'`\"" || isspace(c)
            break
        elseif iscntrl(c)
            next!(error, "Invalid control character ($(repr(c)))")
        else
            commit!()
            push!(word, c)
        end
    end
    CstSymbol(String(word))
end

function isnumber(c, base=10)
    '0' <= c && c - base < '0' ||
        'a' <= c && c - base + 10 < 'a' ||
        'A' <= c && c - base + 10 < 'A'
end

function number(c, base=10)
    '0' <= c && c - base < '0' && return c - '0'
    'a' <= c && c - base + 10 < 'a' && return c - 'a' + 10
    'A' <= c && c - base + 10 < 'A' && return c - 'A' + 10
end

"Returns the integer value of a string. Does not validate its input."
function numberof(string, base=10)
    result = 0
    for c in string
        result = result * base + number(c)
    end
    result
end

function decimalof(string, base=10)
    result = 0.0
    for c in reverse(string)
        result = (result + c - '0') / base
    end
    result
end

const invoketable = Dict{Cst,Function}()
@fold function invokefunc()
    (case = next!(token)) isa CstInt ? next!(parsenumber, case._1) :
    next!(invoketable[case])
end

invoketable[Comment] = @fold function invokecomment()
    string = string(next!(parsesymbol), '.')
    while (!startswith(SubString(obj, state), string))
        next!()
    end
    for _ in string
        next!()
    end
    next!(expr)
end

invoketable[OpenMap] = @fold function invokeset()
    CstCons(CstSymbol("syntax/set"), next!(list, "map", CloseMap))
end

invoketable[CstSymbol("_")] = @fold function invokediscard()
    next!(expr)
    next!(expr)
end
