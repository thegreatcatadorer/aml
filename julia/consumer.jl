# Creates an iterator passed around within functions
# Accessible via 'obj' as the complete state and 'state' as the position
# Internal functions must be called as `next!(func, arg1, arg2)`
# `next!()` with no arguments takes the next element of the iterator
# Used for the LL parser

function compilefold(expr::Expr)
    local mayreturn = false
    function rewrite(obj, state, peekstate, expr)
        isa(expr, Expr) || return expr
        recur(expr) = rewrite(obj, state, peekstate, expr)
        function rewriteget(func=:iterate, arguments...; outstate=state)
            @gensym result
            getexpr = Expr(:call, func, obj, state, arguments...)
            assignexpr = Expr(:(=), Expr(:tuple, result, outstate), getexpr)
            Expr(:block, assignexpr, result)
        end
        if expr.head == :call
            local func = args(expr)[1]
            func == :next! ? (return rewriteget(recur.(args(expr)[2:end])...)) :
            func == :peek! && return rewriteget(recur.(args(expr)[2:end])...; outstate=peekstate)
            func == :commit! && return Expr(:(=), state, peekstate)
            func == :isany && return Expr(:call, :isnothing, Expr(:call, :iterate, obj, state))
        elseif expr.head == :return
            mayreturn = true
            local result = args(expr)[1]
            local exception = Expr(:call, :ReturnException, result)
            return Expr(:call, :throw, exception)
        end
        Expr(expr.head, recur.(args(expr))...)
    end
    function catchreturn(expr)
        @gensym except
        tocatch = Expr(:., except, QuoteNode(:value))
        isexcept = Expr(:call, :isa, except, :ReturnException)
        tocatch = Expr(:if, isexcept, tocatch, Expr(:call, :rethrow))
        Expr(:try, expr, except, Expr(:block, tocatch))
    end
    local obj, state = :obj, :state
    @gensym peekstate
    local decl, body = args(expr)
    local func, arguments... = args(decl)
    body = rewrite(obj, state, peekstate, body)
    mayreturn && (body = catchreturn(body))
    body = Expr(:tuple, body, state)
    body = Expr(:let, peekstate, Expr(:block, body))
    decl = Expr(:call, func, obj, state, arguments...)
    Expr(:(=), decl, body)
end

macro fold(body)
    esc(compilefold(body))
end
