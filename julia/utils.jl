values(args) = filter(a -> !(a isa LineNumberNode), args)
args(expr::Expr) = values(expr.args)

function quoteexpr(expr)
    expr isa Expr || return QuoteNode(expr)
    expr.head == :$ && return args(expr)[1]
    Expr(:call, :Expr, QuoteNode(expr.head), quoteexpr.(expr.args)...)
end

function resolvequotes(expr)
    expr isa Expr || return expr
    expr.head == :quote || return expr
    quoteexpr(Expr(expr.head, resolvequotes.(expr.args)...))
end

"""
iterateor(obj, state, default)

Advances the iterator from `state` in `obj`, returning a tuple of `default`
and the existing state if the iterator is empty.
"""
iterateor(obj, state, default) =
    (result = iterate(obj, state)) !== nothing ? result : (default, state)

macro charoccursin(string)
    @gensym char
    cases = :(false)
    for c in string
        # there'd be a lot of branches if we used ||
        cases = :(($char == $c) | $cases)
    end
    :(($char::Char,) -> $cases)
end

macro some(expr)
    @gensym result
    quote
        $result = $expr
        if $result === nothing
            return nothing
        else
            $result
        end
    end
end

atindex(i::UInt) = (t) -> t[i]
atindex(::Val{i}) where i = (t) -> t[i]

struct ReturnException{T} <: Exception
    value::T
end