# Select argument bytes from an integer.
head(i) = i & 255
first(i) = (i & (255 << 8)) >> 8
second(i) = (i & (255 << 16)) >> 16
third(i) = (i & (255 << 24)) >> 24
fourth(i) = (i & (255 << 32)) >> 32
fifth(i) = (i & (255 << 40)) >> 40
sixth(i) = (i & (255 << 48)) >> 48
seventh(i) = (i & (255 << 56)) >> 56

function assertin(i, l)
    max = (((1 << (l - 1)) - 1) << 1) + 1
    @assert (i == (i & max)) "$(i) out of bounds; exceeded $(max)!"
end

function makecmd(args...)
    result = 0
    offset = 0
    for a in args
        assertin(a, 8)
        result |= a << offset
        offset += 8
    end
    @assert (offset <= 64) "too many arguments in a command!"
end

struct EvalSuccess end

function bytecode_eval(extern::Vector{Function}, env::Vector{Int}, main::Int)
    stack = zeros(Int, 65536)
    allocs = Dict{Int,Vector{Int}}()
    counter = 0
    function run(cmd::Int, ip::Int, bp::Int)::EvalSuccess
        get(r) = stack[bp+r]
        set(r, i) = stack[bp+r] = i
        pred(b) = ip + (b ? 1 : 2)
        while true
            case = head(cmd)
            if case == 0 #= No-op =#
                cmd, ip, bp = env[ip+1], ip + 1, bp
            elseif case == 1 #= Call =#
                set(first(cmd), ip + 2)
                dest = cmd << 24
                cmd, ip, bp = env[dest], dest, bp + second(cmd)
            elseif case == 2 #= Enter =#
                cmd, ip, bp = cmd >> 16, ip, bp + first(cmd)
            elseif case == 3 #= Exit =#
                cmd, ip, bp = cmd >> 16, ip, bp - first(cmd)
            elseif case == 4 #= Point =#
                set(first(cmd), ip + second(cmd))
                cmd, ip, bp = cmd >> 24, ip, bp
            elseif case == 5 #= Follow =#
                dest = get(first(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 6 #= Jump =#
                dest = cmd << 8
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 7 #= Extern =#
                act::Function = extern[cmd<<8]
                act(bp, stack)
                cmd, ip, bp = env[ip+1], ip + 1, bp
            elseif case == 8 #= Alloc =#
                position = counter += 1
                size = get(second(cmd)) * third(cmd)
                array = zeros(Int, size)
                allocs[position] = array
                set(first(cmd), position)
                cmd, ip, bp = cmd >> 32, ip, bp
            elseif case == 9 #= Free =#
                delete!(allocs, get(first(cmd)))
                cmd, ip, bp = cmd >> 16, ip, bp
            elseif case == 10 #= Realloc =#
                position = get(first(cmd))
                size = get(second(cmd)) * third(cmd)
                source = allocs[position]
                target = zeros(Int, size)
                range = 1:min(length(target), length(source))
                @simd for i in range
                    @inbounds target[i] = source[i]
                end
                allocs[position] = target
                cmd, ip, bp = cmd >> 32, ip, bp
            elseif case == 11 #= IndexConstant =#
                array = allocs[get(second(cmd))]
                set(first(cmd), array[get(third(cmd))])
                cmd, ip, bp = cmd >> 32, ip, bp
            elseif case == 12 #= IndexRegister =#
                array = allocs[get(second(cmd))]
                set(first(cmd), array[third(cmd)])
                cmd, ip, bp = cmd >> 32, ip, bp
            elseif case == 13 #= SetIndexConstant =#
                array = allocs[get(second(cmd))]
                array[get(third(cmd))] = get(first(cmd))
                cmd, ip, bp = cmd >> 32, ip, bp
            elseif case == 14 #= SetIndexRegister =#
                array = allocs[get(second(cmd))]
                array[third(cmd)] = get(first(cmd))
            elseif case == 15 #= LoadChar =#
                set(first(cmd), second(cmd))
                cmd, ip, bp = cmd >> 24, ip, bp
            elseif case == 16 #= LoadInteger =#
                set(first(cmd), cmd >> 16)
                cmd, ip, bp = env[ip+1], ip + 1, bp
            elseif case == 17 #= LoadLong =#
                set(first(cmd), env[ip+1])
                cmd, ip, bp = env[ip+2], ip + 2, bp
            elseif case == 18 #= Switch =#
                dest = ip + get(first(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 19 #= SwitchMod =#
                dest = ip + %(get(first(cmd)), second(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 20 #= SwitchElse =#
                dest = ip + min(get(first(cmd)), second(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 21 #= WhenZero =#
                dest = pred(get(first(cmd)) == 0)
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 22 #= WhenEqualRegister =#
                dest = pred(get(first(cmd)) == get(second(cmd)))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 23 #= WhenEqualConstant =#
                dest = pred(get(first(cmd)) == second(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 24 #= WhenLessRegister =#
                dest = pred(get(first(cmd)) < get(second(cmd)))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 25 #= WhenLessConstant =#
                dest = pred(get(first(cmd)) < second(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 24 #= WhenMoreRegister =#
                dest = pred(get(first(cmd)) > get(second(cmd)))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 25 #= WhenMoreConstant =#
                dest = pred(get(first(cmd)) > second(cmd))
                cmd, ip, bp = env[dest], dest, bp
            elseif case == 26 #= AddRegister =#
                set(first(cmd), get(second(cmd)) + get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 27 #= AddConstant =#
                set(first(cmd), get(second(cmd)) + third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 28 #= MulRegister =#
                set(first(cmd), get(second(cmd)) * get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 29 #= MulConstant =#
                set(first(cmd), get(second(cmd)) * third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 30 #= SubRegisters =#
                set(first(cmd), get(second(cmd)) - get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 31 #= SubLeftConstant =#
                set(first(cmd), second(cmd) - get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 32 #= SubRightConstant =#
                set(first(cmd), get(second(cmd)) - third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 33 #= DivRegisters =#
                set(first(cmd), get(second(cmd)) / get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 34 #= DivLeftConstant =#
                set(first(cmd), second(cmd) / get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 35 #= DivRightConstant =#
                set(first(cmd), get(second(cmd)) / third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 36 #= RemRegisters =#
                set(first(cmd), get(second(cmd)) % get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 37 #= RemLeftConstant =#
                set(first(cmd), second(cmd) % get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 38 #= RemRightConstant =#
                set(first(cmd), get(second(cmd)) % third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 39 #= AndRegister =#
                set(first(cmd), get(second(cmd)) & get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 40 #= AndConstant =#
                set(first(cmd), get(second(cmd)) & third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 41 #= IorRegister =#
                set(first(cmd), get(second(cmd)) | get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 42 #= IorConstant =#
                set(first(cmd), get(second(cmd)) | third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 43 #= XorRegister =#
                set(first(cmd), xor(get(second(cmd)), get(third(cmd))))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 44 #= XorConstant =#
                set(first(cmd), xor(get(second(cmd)), third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 45 #= ShiftRegisters =#
                set(first(cmd), get(second(cmd)) << get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 46 #= ShiftLeftConstant =#
                set(first(cmd), second(cmd) << get(third(cmd)))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 47 #= ShiftRightConstant =#
                set(first(cmd), get(second(cmd)) << third(cmd))
                cmd, ip, bp = cmd >> 32, dest, bp
            elseif case == 48 #= Not =#
                set(first(cmd), ~get(second(cmd)))
                cmd, ip, bp = cmd >> 24, dest, bp
            elseif case == 49 #= Exit =#
                return EvalSuccess()
            else
                @assert false "out of range! cmd = $(cmd), ip = $(ip), bp = $(bp)"
            end
        end
    end
    _ = run(env[main], main, 0)
    return
end
