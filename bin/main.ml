open Aml.Parser

let () =
  let input : InChannelInput.t =
    { input = In_channel.stdin; buffer = Buffer.create 0 }
  in
  let state : InChannelParser.state = { input; pos = 0 }
  and env = Base.Queue.create () in
  match InChannelParser.parse_toplevel env state with
  | Error _ -> raise Exit
  | Ok (_state, expr) ->
      print_endline "recieved!";
      print_endline (InChannelParser.display expr)
