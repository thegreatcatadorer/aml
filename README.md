# AML

A prototype for a compiled, yet interactive language with type-aware macros. It SHOULD NOT BE USED IN PRODUCTION (not that it's anywhere near complete enough).

```clj
(defmodule Fib ; define a module named Fib
  {A Type ; parameterized by a type A
  nat (Nat A) ; and an implementation of Nat for A
  zero? (Fn [A] Bool) ; and a predicate for zero-ness
  one? (Fn [A] Bool) ; and one for one-ness
  } ; wherein, we
  (defn fib ; define a function name fib
    {iter A} ; on `iter`, a value of type A
	A ; yielding a value of type A
	; which is
	(if (or iter.zero? iter.one?) ; if iter is zero or one
	  iter ; iter is the result
	  (+ ; otherwise, the sum of
	    (fib (- iter 1)) ; fib of iter - 1
		(fib (- iter 2)) ; and fib of iter - 2
		))))
```

## Contents

- [Architecture](#architecture)
- [Language](#language)
  - [Syntax](#syntax)
  - [Control Flow](#control-flow)
  - [Types](#types)
  - [Standard Interfaces](#standard-interfaces)
  - [Functions, Modules, and Macros](#functions-modules-and-macros)
  - [Usage](#usage)

## Architecture

`README.md`: Description, project documentation, and language documentation.

`bin/main.ml`: Program executable; a minimal program connecting the components in -

`lib`: Project components, including the below:

- `eval.ml`: Bytecode interpreter.
- `expander.ml`: Macro expansion and type-checking of the CST, emits SSA.
- `parser.ml`: Compiler frontend - a recursive-descent lexer and parser.
- `structured.ml`: Definition of the SSA format, along with a register allocator.
- `utils.ml`: Useful functions for the implementation of the other libraries.

`archive`: Previous designs for internal libraries; largely non-operational.

## Language

### Syntax

AML's syntax is largely derived from Clojure's, but there are significant changes to semantics, in line with its low-level nature:

- Function calls are delimited by `()`; quoted, they form immutable linked lists.
- Arrays compile to a call of `Syntax:array` on their elements, and are mutable and bounds-checked.
- Maps, implemented by mutable hash tables, compile to a call of `Syntax:map`.
- Sets are compiled to a call of `Syntax:set`, yielding hash sets.

AML maintains support for `#_`, `#Date`, and `#re`, but adds arbitrary, user-implemented readers, along with `:`, a left-associative infix operator (yes, heresy), which serves as a shorthand for reversed binary calls, is required to be unseparated from its left operand (it otherwise begins a keyword), and is used for both namespacing and field access.

### Control Flow

AML, like many other languages, uses strict control flow: where an expression's dependencies are evaluated before the expression, regardless of whether they affect its value. Similar to other strict languages, it provides sequencing and conditional forms.

- `do` evaluates a series of forms in sequence, preserving lexical context between them; a corresponding `begin` form executes each statement in a unique scope.
- `if` evaluates the first expression to a `Bool`; if it is `true`, then it evaluates to the second expression, whereas it evaluates to the third expression if the first is `false`.
- `when` evaluates the first expression, a `Bool`, and if it is `true`, evaluates the following expressions in sequence, as in a `do` block.
- `match` implements pattern matching for a type; It is implemented by way of `do`, `if`, `def`, and the `unreachable` statement. Matching is defined for most default type constructors and predicates, but implementations can be added by way of an `expand-match` property in a module.
- `loop`, using initial values for its arguments, rewrites calls of `return` to be a recursive call of a function; it is equivalent to a local function definition.
- `for` performs an expression for each value in its argument, expected to be an iterator, binding values in its scope by way of a pattern-match; if the match would fail, the `for` loop terminates.
- `block` rewrites its body so that any calls of `return` within result in that value being returned from the block; its expansion only uses structured constructs (e.g. `do`, `if`, `loop`).
- `->`, given a sequence of expressions, uses the first as an initial value and loops over the remainder of its arguments, replacing the current value with, if the current expression is syntactically a function call, that expression with the current value inserted as its first argument, and otherwise with a call of that expression on the current value.
- `->>` does the same, but inserts the current value as the last argument, rather than the first.
- `unreachable` designates a path as one the compiler should assume cannot happen, and should be in a return position. **Use of `unreachable` can cause silently wrong results or undefined behavior.**

### Types

AML is statically typed; all names are associated with a subset of possible values.

The type system supports naturals (`N`-prefixed) and integers (`I`-prefixed) with power-of-two sizes [8, 128], along with the pointer-sized numbers `NSize` and `ISize`; these hierarchies are abstracted over by the `Int` and `Nat` types. Booleans `true, false : Bool`, the more general `Bitset` type, and a zero-sized type `()` are also provided in the global namespace. Unit values may have related semantics within the program; these values can be represented as `:keywords`, are their own types, and may also be used to carry constant information.

To allow more complex structures to be created from these primitives, AML's type system has a runtime representation of types and functions acting on it, including:

- `All` combines several types into a structure where they are adjacent, equiv. to a tuple, and is displayed with `[]`.
- `Any` stores one of the member types, along with a tag specifying which.
- `Record` and `Result` resemble these, but name their inhabitants, with accessors implicitly defined; `Record` is displayed with `{}`.
- `Vector` stores a constant number of homogenous values.
- `Ref` is a word-sized reference to a value and allows performing a bitwise copy.
- `Ptr` differs from `Ref` in that it can mutate the referenced value.
- `Box` differs from `Ref` in that it indicates ownership - it should either be passed to a function that consumes a `Box` or dropped.
- `Cell` combines the properties of `Ptr` and `Box`.
- `Option` is an interface for optional types, and includes:

  - `Maybe` is repr. equiv. to `Any` of `()` and the type.
  - `Except` interprets a fixed value of the type as `None`.
  - `Nullable` is repr. equiv. to `Except` of a `Ref`.
- `LinkedList` is a linked list composed of `Nullable` references, displayed with `(linked-list)`.
- `List` is a list represented by a dynamic array, displayed with `(list)`.
- `Slice` is a reference to part of a `List`, containing both the selected start index and its length.
- `HashMap` is a mutable hash map, displayed with `(map)`.
- `HashSet` is a mutable hash set, displayed with `(set)`.
- `Array` is an unsized, mutable array.
- `Union` is a combination of interfaces.

Additionally, `Has` represents an unknown type with an interface implemented by a dispatch table; It is useful where monomorphization would be a concern for efficiency and where a function might receive heterogeneous values in a collection.

The related `Dynamic` will perform lookup for functions called on it and generate them if necessary, making it unable to use many of the benefits of compilation, and should only be used for interactive purposes, e.g. scripting and configuration.

### Standard Interfaces

AML provides polymorphism using interfaces, and defines several of these up front, though any type can be used as one.

- `Nat` represents a bounded natural number. `Nat` values support addition `+`, subtraction `-`, multiplication `*`, Euclidean division `div`, and two's-complement wrapping, saturating, and checked versions of each. They also support modulus `mod`, remainder `rem`, bitwise and `logand`, or `logior`, exclusive or `logxor`, not `lognot`, left shift `logshl`, right shift `logshr`, and arithmetic right shift `arishr`.
- `Int` represents a bounded integer. `Int` values support all arithmetic operations of `Nat`, along with modulus `mod`, remainder `rem`, negation `neg`, and absolute value `abs`.

Both `Nat` and `Int` must have total orders, with comparison by `cmp`, `<`, `=`, `>`, `<=`, `>=`, `increasing?`, `decreasing?`, `strict-increasing?`, `strict-decreasing?`, `equal?`, and the related `max` and `min` functions.

- `Float` represents a floating-point value. `Float` values support addition, subtraction, multiplication, real division '/', exponentiation, logarithms, and trigonometric functions (with inputs in turns). `Float` values only require a partial order.
- As shown above, `Option` is an interface that includes the `Maybe` data type, `Nullable` references, and types with `Except`ional values. `Option` values have generic `match` and `do` macros and the functions `map`, `select`, and `select-else`. To create an `Option` of a type, if no specific `Option` implementor is needed, use the type's `some` or `none` methods.
- The `Container` interface allows selecting a value of a type using a value of another. `Array`, `List`, and `Tuple` all implement `Container Nat`, while `HashMap` implements it for the type of its keys.
- The `From` interface creates a value of the target type from a similar value of the argument type; it is implemented from larger to smaller intrinsic `Int` and `Nat` types, between `Ptr` and `Ref`, and from `List` to both `Array` and `Slice`.
- `OptionFrom` is similar, but returns an `Option`, and is implemented from smaller to larger `Int` and `Nat` types, and from `Float` to `Int`.
- `Stream` is a stateless iterator, created by the `seq` function; `(first stream)` is the key to the first value in a stream, `(stream key)` retrieves the value of the stream at that key, and `(next key stream)` returns an `Option` of the next key in the stream.
- `Iterator` is a stateful iterator, created by the `iterate` function; `(empty? iterator)` asks whether the iterator can yield another element, `(next! iterator)` returns the next element or exits, and `(next-option! iterator)` returns an `Option` of the next element.

### Functions, Modules, and Macros

Toplevel functions in AML represent functions from one concrete type (a record of values, often destructured) to another (the result, also in a type context). To create generic functions over a restricted set of types, define a function for each type in the set. To define functions over any type, or any type that has certain properties, use the `defmodule` form; It creates a module function.

Modules are functions evaluated to provide definitions of functions and variables. Unlike functions, modules can take types as arguments and use them in their own arguments or those of their inner functions. Anything in the module that is documented is public, including module inclusions. (The rationale for this is that for undocumented functions one would need to inspect the internals of the module and break encapsulation, rendering scoping modifiers meaningless, and that internal documentation can be accomplished with comments, which are visible when inspecting the source.) Module members can be accessed either by calling a function with the module as its first argument or by using a `use` declaration within a local scope; however, invoking a module adds any names to the current scope within that are not defined locally, such that many functions may be used with only a partial path.

AML supports macros, but separates their definition from that of functions. They may be defined by `(define-syntax! macro-name implementing-function-name)`, where `implementing-function-name` has a method for `[Expr]`, and also locally set with `Syntax-def`, though local macros cannot use locally defined values other than macros. AML also supports reader-macros, which are `(Fn [String] Expr)` and are defined, using `define-reader-for!`, similarly to macros.

### Usage

```clj
(defmodule DynArray {Elem Type}
	"A dynamic array (re)implementation."
	(Record It :length NSize :capacity NSize :items Elem:Array)
	(defn get-option {array It, position NSize} Elem:Ref:Option
		"Selects an item at a given position in an array. If the item is contained within, returns a reference to the item; otherwise, returns a null reference."
		(if (< position array:length)
			(do (def array-start (Ref:of array:items))
				; pointer arithmetic, potentially unsafe
				(def array-elem (+ array-start position))
				(some array-elem))
			Elem:Ref:Option:none))
	(defn get {array It, position NSize} Elem:Ref
		"Selects an item at a given position in an array. If the item is contained within, returns a reference to the item; otherwise, fails."
		(assert (< position array:length))
		(def array-start (Ref:of array:items))
		(+ array-start position))
	(defn invoke {position NSize, array It} Elem:Ref:Option
		"Selects an item at a given position in an array. If the item is contained within, returns a reference to the item; otherwise, returns a null reference."
		(get-option array position))
	(defn set! {array It:Ptr, position NSize, update Elem} 
		{:success Bool :free Elem}
		"If an item is found at the position within the array, returns that item and replaces it with the update. Returns the success of the operation along with the remaining item.
		Usage:
		(def array (NSize:DynArray:new 3 5))
		(set! array 2 3)
		array ; = [5 5 3]"
		(def array array-ptr:get)
		(if (< position array:length)
			(do (def array-start (Ptr:of array:items))
				(def elem-ptr (+ array-start position))
				(def item (swap! elem-ptr update))
				{:success true, :free item})
			{:success false, :free update}))
	(defn conj!
		{array It:Ptr
		elem Elem
		position NSize
		[copy :default] (Fn [A] A)
		[free :default] (Fn [A] ())}
		()
		"Adds an item to the end of a dynamic array. If the array has as many elements as it can store, it reallocates, copies elements over, and replaces the stored array.
		Usage:
		(def array (create (NSize:DynArray:new 3 5)))
		(conj! array 4)
		array ; = [5 5 5 4]"
		(def array array-ptr:get)
		(match (set! array:position elem)
			{:success true :free free-elem} (do 
				(modify! array-ptr:get:length inc)
				free-elem:free)
			_ (do (def capacity1 (inc (* 2 array:capacity)))
		  		(def items1 (Elem:Array:new capacity1))
				(copy! array:items items1 :up-to capacity)
				(free (swap! array-ptr:get:items items1))
				(set! array-ptr:get:capacity capacity1)
				(recur array-ptr elem position copy free))))
	(defn search {array It, pred? (Fn [Elem] Bool)} Elem:Ref:Option
		"Performs a linear search over the array.
		Usage:
		(defn =3 {i Int} Bool (= i 3))
		(def array (create (NSize:DynArray:new 3 5)))
		(conj! array 3)
		(search array =3) ; = (some (ref 3))"
		(loop {i (unsigned 0)}
			(if (= i array:length) Elem:Ref:Option:none
				(if (pred? (get:unchecked array i))
					(Elem:Ref:Option:some i)
					(recur i:inc)))))
	(defn binary-search {array It, [cmp :default] (Fn [Elem] Cmp)} Elem:Option
		(loop {low (unsigned 0), high (unsigned array:length:dec)}
			(if (= low high) Elem:Option:none
				(do (def mid (midpoint low high))
					(case mid:cmp
						{less (recur low mid:dec)
						more (recur mid:inc high)
						equal (Elem:Option:some array:mid})))))
	(def Seq (Record :contents It))
	(defn seq {array It} Seq
		{:contents array})
	(defn first {seq Seq} NSize
		0:NSize)
	(defn {seq Seq, key NSize} Elem:Ref:Option
		seq:contents:key)
	(defn next {key NSize, seq Seq} NSize
		key:inc)
	(defn new {} It
		"Initializes a dynamic array with a length of 0. It does no allocation; that occurs on insertion.
		Usage: (NSize:DynArray:new)"
		{:length 0, :capacity 0, :items Elem:Array:null})
	(defn new {length NSize} It
		"Creates a dynamic array with length passed as an argument. It will immediately allocate.
		Usage: (NSize:DynArray:new 5)"
		{:length 0, :capacity length, :items (Elem:Array:new length)})
	(defn new {length NSize, default Elem, [copy :default] (Fn [Elem] Elem)} It
		"Creates a dynamic array filled with copies equal to length of the default.
		Usage: (NSize:DynArray:new 5 3)"
		(def array (Elem:Array:new length))
		(for i (range=< 0 length)
			(set! array:i default:copy))
		{:length length, :capacity length, :items array})
	(defn of {source Elem:List, [copy :default] (Fn [Elem] Elem)} It
		"Copies a builtin list into a DynArray. Does not consume the argument."
		(def length source:length)
		(def array (Elem:Array:new length))
		(for i (range=< 0 length) (set! array:i array:i:copy))
		{:length length, :capacity length, :items array})
	(defn free {array It} {}
		(if array:items:null? {}
			array:items:free)))
```
